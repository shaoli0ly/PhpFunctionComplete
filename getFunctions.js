// https://www.php.net/manual/zh/indexes.functions.php // 函数
// https://www.php.net/manual/zh/reserved.variables.php // 预定义变量
// 不完整，完整版键getFuncitons.php
var funs = {};
var idxList = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','_'];
for (var i = 0; i < idxList.length; i++) {
    var refentry = document.querySelector('#refentry-index-for-' + idxList[i]);
    var lis = refentry.querySelectorAll('li');
    funs[idxList[i]] = {};
    for (var j = 0; j < lis.length; j++) {
        var a = lis[j].querySelector('a');
        var url = a.getAttribute('href');
        if (url.indexOf('function') == -1) {
            continue;
        }
        var fun = a.innerText;
        funs[idxList[i]][fun] = {
            'url' : url,
            'fun' : fun,
            'desc' : lis[j].innerText,
        };
    }
}
console.log(JSON.stringify(funs));